"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const logSettings_1 = require("@typegoose/typegoose/lib/logSettings");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.setGlobalPrefix('api');
    await app.listen(3000);
}
bootstrap().catch(error => { logSettings_1.logger.error(error, null, 'Bootstrap'); });
//# sourceMappingURL=main.js.map