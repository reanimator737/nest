import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { logger } from '@typegoose/typegoose/lib/logSettings';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.setGlobalPrefix('api');
	await app.listen(3000);
}

bootstrap().catch(error => { logger.error(error, null, 'Bootstrap'); });



